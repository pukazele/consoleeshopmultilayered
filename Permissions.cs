﻿using System;

public enum Permissions
{
    ShowProducts,
    SearchProduct,
    Register,
    Login,
    MakeOrder,
    ShowOrders,
    EditProfile,
    Exit,
    ManageUsers,
    AddProducts,
    ChangeProductsInfo,
    ManageOrders
}
