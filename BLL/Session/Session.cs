﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL;

namespace BLL
{
    public class Session
    {
        private readonly UnitOfWork database;
        public UserDTO CurrentUser { get; set; }

        public OrderService OrderService { get; }
        public ProductService ProductService { get; }
        public UserService UserService { get; }

        public Session()
        {
            database = new UnitOfWork();
            CurrentUser = new UserDTO();
            LogOut();

            OrderService = new OrderService(database);
            ProductService = new ProductService(database);
            UserService = new UserService(database);
        }

        public string LogIn(UserDTO user)
        {
            try
            {
                CurrentUser = UserService.GetItemByLogAndPass(user.Login, user.Password);
                return "Authorization successful!";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public void LogOut()
        {
            var entryUser = new GuestUser();

            CurrentUser.Id = entryUser.Id;
            CurrentUser.Nickname = entryUser.Nickname;
            CurrentUser.Permissions = entryUser.Permissions;
        }

        public List<string> ShowProducts()
        {
            var result = new List<string>();

            foreach (var item in ProductService.GetAll())
            {
                string listItem = "ID: " + item.Id + " | " + "Product: " + item.Name + " | " + "Price: " + item.Price;
                result.Add(listItem);
            }

            return result;
        }

        public List<string> SearchProduct(string search)
        {
            
            var result = new List<string>();

            try
            {
                foreach (var item in ProductService.GetItemsByName(search))
                {
                    string listItem = "ID: " + item.Id + " | " + "Product: " + item.Name + " | " + "Price: " + item.Price;
                    result.Add(listItem);
                }
            }
            catch (Exception ex)
            {
                result.Add(ex.Message);
            }

            return result;

        }

        
    }
}
