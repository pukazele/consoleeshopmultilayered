﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL;

namespace BLL
{
    public class OrderDTO : IDTO
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public IUser User { get; set; }

        public Dictionary<int, int> ProductIdAndAmountPairs { get; set; }
        public List<IProduct> ProductsList { get; set; }

        private decimal totalPrice = 0;
        public decimal TotalPrice
        {
            get
            {
                decimal result = 0;

                foreach (var product in ProductsList)
                {
                    foreach (var pair in ProductIdAndAmountPairs)
                    {
                        if (product.Id == pair.Key) result += product.Price * pair.Value;
                    }
                }

                return result;
            }
        }

        public OrderStatus Status { get; set; } = OrderStatus.New;
    }
}
