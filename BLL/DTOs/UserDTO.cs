﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public class UserDTO : IDTO
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public List<int> OrdersId { get; set; }
        public List<Order> Orders { get; set; }
        public IPermissions Permissions { get; set; }
    }
}
