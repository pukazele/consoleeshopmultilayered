﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL;

namespace BLL
{
    public class ProductDTO : IDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public List<int> OrdersId { get; set; }
        public List<Order> Orders { get; set; }
    }
}
