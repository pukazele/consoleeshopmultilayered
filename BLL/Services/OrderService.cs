﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL;

namespace BLL
{
    public class OrderService : IService<OrderDTO>
    {
        UnitOfWork Db { get; set; }

        public OrderService(UnitOfWork db)
        {
            Db = db;
        }

        public IEnumerable<OrderDTO> GetAll()
        {
            foreach (var item in Db.Orders.GetItems())
            {
                yield return ToOrderDTO(item);
            }
        }

        public OrderDTO GetItemById(int id)
        {
            return ToOrderDTO(Db.Orders.GetItem(id));
        }

        public void AddItem(OrderDTO order)
        {
            var newOrder = new Order()
            {
                ProductIdAndAmountPairs = order.ProductIdAndAmountPairs,
                ProductsList = order.ProductsList,
                User = order.User,
                UserId = order.UserId
            };

            Db.Orders.Create(newOrder);
        }

        public void UpdateItem(OrderDTO order)
        {
            var updatedOrder = new Order()
            {
                Id = order.Id,
                ProductIdAndAmountPairs = order.ProductIdAndAmountPairs,
                ProductsList = order.ProductsList,
                Status = order.Status,
                User = order.User,
                UserId = order.UserId
            };

            Db.Orders.Update(updatedOrder);
        }

        private OrderDTO ToOrderDTO(Order order)
        {
            var result = new OrderDTO()
            {
                Id = order.Id,
                ProductIdAndAmountPairs = order.ProductIdAndAmountPairs,
                ProductsList = order.ProductsList,
                Status = order.Status,
                User = order.User,
                UserId = order.UserId
            };

            return result;
        }
    }
}
