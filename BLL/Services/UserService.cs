﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL;
using System.Linq;

namespace BLL
{
    public class UserService : IService<UserDTO>
    {
        UnitOfWork Db { get; set; }

        public UserService(UnitOfWork db)
        {
            Db = db;
        }

        public IEnumerable<UserDTO> GetAll()
        {
            foreach (var item in Db.Users.GetItems())
            {
                yield return ToUserDTO(item);
            }
        }

        public UserDTO GetItemById(int id)
        {
            var search = Db.Users.GetItem(id);

            return ToUserDTO(search);
        }

        public UserDTO GetItemByLogAndPass(string login, string password)
        {
             return ToUserDTO(Db.Users.GetItems().FirstOrDefault(x => x.Login == login && x.Password == password)) 
                ?? throw new ArgumentException("Login or Password is uncorrect");
        }

        public void AddItem(UserDTO userToAdd)
        {
            var newUser = new RegisteredUser()
            {
                Login = userToAdd.Login,
                Password = userToAdd.Password,
                Nickname = userToAdd.Nickname,
                Orders = userToAdd.Orders,
                OrdersId = userToAdd.OrdersId,
            };

            Db.Users.Create(newUser);
        }

        public void UpdateItem(UserDTO userToUpdate)
        {
            var updatedUser = new RegisteredUser()
            {
                Id = userToUpdate.Id,
                Login = userToUpdate.Login,
                Password = userToUpdate.Password,
                Nickname = userToUpdate.Nickname,
                Orders = userToUpdate.Orders,
                OrdersId = userToUpdate.OrdersId,
            };

            Db.Users.Update(updatedUser);
        }

        private UserDTO ToUserDTO(RegisteredUser entryUser)
        {
            var userDTO = new UserDTO()
            {
                Id = entryUser.Id,
                Login = entryUser.Login,
                Password = entryUser.Password,
                Nickname = entryUser.Nickname,
                Orders = entryUser.Orders,
                OrdersId = entryUser.OrdersId,
                Permissions = entryUser.Permissions
            };

            return userDTO;
        }
    }
}
