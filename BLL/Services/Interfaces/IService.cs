﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public interface IService<TDTO> where TDTO : class
    {
        IEnumerable<TDTO> GetAll();
        public TDTO GetItemById(int id);
        public void AddItem(TDTO itemToAdd);
        public void UpdateItem(TDTO itemToUpdate);
    }
}
