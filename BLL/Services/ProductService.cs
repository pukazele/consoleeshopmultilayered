﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL;
using System.Linq;
using System.Text.RegularExpressions;

namespace BLL
{
    public class ProductService : IService<ProductDTO>
    {
        UnitOfWork Db { get; set; }

        public ProductService(UnitOfWork db)
        {
            Db = db;
        }

        public IEnumerable<ProductDTO> GetAll()
        {
            foreach (var prod in Db.Products.GetItems())
            {
                yield return ToProductDTO(prod);
            }
        }

        public ProductDTO GetItemById(int id)
        {
            var search = Db.Products.GetItem(id);

            return ToProductDTO(search);
        }

        public IEnumerable<ProductDTO> GetItemsByName(string namePart)
        {
            int count = 0;
            string pattern = @"\w*" + namePart + @"\w*";
            var reg = new Regex(pattern, RegexOptions.IgnoreCase);

            foreach (var item in Db.Products.GetItems())
            {
                var match = reg.Match(item.Name);

                if (match.Success)
                {
                    count++;
                    yield return ToProductDTO(item);
                }
            }

            if (count == 0) throw new ArgumentException("Its no product(s) with provided name");
        }

        public void AddItem(ProductDTO prodToAdd)
        {
            var newProd = new Product()
            {
                Name = prodToAdd.Name,
                Orders = prodToAdd.Orders,
                OrdersId = prodToAdd.OrdersId,
                Price = prodToAdd.Price
            };

            Db.Products.Create(newProd);
        }

        public void UpdateItem(ProductDTO prodToUpdate)
        {
            Db.Products.Update(ToProduct(prodToUpdate));
        }

        private Product ToProduct(ProductDTO entryProdDTO)
        {
            var result = new Product()
            {
                Id = entryProdDTO.Id,
                Name = entryProdDTO.Name,
                Orders = entryProdDTO.Orders,
                OrdersId = entryProdDTO.OrdersId,
                Price = entryProdDTO.Price
            };

            return result;
        }

        private ProductDTO ToProductDTO(Product entryProd)
        {
            var result = new ProductDTO()
            {
                Id = entryProd.Id,
                Name = entryProd.Name,
                Orders = entryProd.Orders,
                OrdersId = entryProd.OrdersId,
                Price = entryProd.Price
            };

            return result;
        }
    }
}
