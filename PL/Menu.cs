﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL;
using System.Linq;

namespace PL
{
    public class Menu
    {
        public Session Session { get; set; }

        public Menu()
        {
            Session = new Session();
        }

        public void ShowProducts()
        {
            Console.Clear();
            var prodMenu = Session.ShowProducts();

            foreach (var item in prodMenu)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
            Console.WriteLine("Enter 0 for exit to main menu.");

            WaitForExit();
        }

        public void SearchProducts()
        {
            Console.Clear();
            Console.WriteLine("Enter keywords");
            string input = Console.ReadLine();
            var searched = Session.SearchProduct(input);

            Console.Clear();

            foreach (var item in searched)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
            Console.WriteLine("Enter 0 for exit to main menu.");

            WaitForExit();
        }

        private void WaitForExit()
        {
            var input = Console.ReadLine();
            if (IsNumber(input) && int.Parse(input) == 0)
            {
                EntryMenu.Start();
            }
            else WaitForExit();
        }

        private bool IsNumber(string str)
        {
            int res;
            return Int32.TryParse(str, out res);
        }
    }
}
