﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL;
using System.Linq;

namespace PL
{
    static class EntryMenu
    {
        public static Menu Menu { get; set; } = new Menu();

        public static Dictionary<int, KeyValuePair<Action, string>> MenuItems { get; set; }

        public static void BuildEntryMenu()
        {
            MenuItems = MenuBuilder.BuildMenu(Menu);
        }

        public static void ChooseMenuItem()
        {
            var input = Console.ReadLine();
            bool isNumber = input.Length == input.Where(c => char.IsNumber(c)).Count();

            if (isNumber)
            {
                var parsedInput = int.Parse(input);
                foreach (var item in MenuItems)
                {
                    if (item.Key == parsedInput) item.Value.Key.Invoke(); 
                }
            }
        }

        public static void ShowEntryMenu()
        {
            Console.Clear();
            foreach (var item in MenuItems)
            {
                Console.WriteLine(item.Key + ". " + item.Value.Value);
            }
        }

        public static void Start()
        {
            BuildEntryMenu();
            ShowEntryMenu();
            ChooseMenuItem();
        }
    }
}
