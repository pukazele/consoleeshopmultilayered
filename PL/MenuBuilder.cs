﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PL
{
    public static class MenuBuilder
    {
        private static int AutoId { get; set; }

        public static Dictionary<int, KeyValuePair<Action, string>> BuildMenu(Menu menu)
        {
            AutoId = 1;

            var result = new Dictionary<int, KeyValuePair<Action, string>>();

            if (menu.Session.CurrentUser.Permissions.ListOfPermissions.Contains(DAL.Permissions.ShowProducts))
            {
                Action deleg = menu.ShowProducts;
                var menuItem = new KeyValuePair<Action, string>(deleg, "Show Products");

                result.Add(AutoId++, menuItem);
            }

            if (menu.Session.CurrentUser.Permissions.ListOfPermissions.Contains(DAL.Permissions.SearchProduct))
            {
                Action deleg = menu.SearchProducts;
                var menuItem = new KeyValuePair<Action, string>(deleg, "Search Products");

                result.Add(AutoId++, menuItem);
            }

            return result;
        }
    }
}
