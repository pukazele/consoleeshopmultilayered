﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class OrdersContext : DBContext
    {
        public DBSet<Order> Orders { get; set; }
        public DBSet<Product> Products { get; set; }
        public DBSet<RegisteredUser> Users { get; set; }

        public OrdersContext() => SimulateConnection();

        public override void SimulateConnection()
        {
            Products = CreateProducts();
            Orders = CreateOrders();
            Users = CreateUsers();
        }

        private DBSet<Order> CreateOrders()
        {
            var simulatedDb = new DBSet<Order>
            {

            };

            return simulatedDb;
        }

        private DBSet<Product> CreateProducts()
        {
            var simulatedDb = new DBSet<Product>
            {
                new Product { Name = "Samsung Galaxy S21", Price = 20000},
                new Product { Name = "Iphone 12 Pro Max", Price = 25000},
                new Product { Name = "OnePlus 7 Pro", Price = 15000},
                new Product { Name = "Google Pixel 5", Price = 18000},
                new Product { Name = "Samsung Galaxy Note 10 Lite", Price = 12500}
            };

            return simulatedDb;
        }

        private DBSet<RegisteredUser> CreateUsers()
        {
            var simulatedDb = new DBSet<RegisteredUser>
            {
                new RegisteredUser() { Login = "1234", Password = "1234", Nickname = "Hailrake"},
                new RegisteredUser() { Login = "aloha", Password = "zxc", Nickname = "Let me die", Permissions = new AdminPermissions()},
            };

            return simulatedDb;
        }
    }
}
