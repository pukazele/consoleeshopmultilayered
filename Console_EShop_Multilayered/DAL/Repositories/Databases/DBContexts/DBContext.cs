﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public abstract class DBContext
    {
        public abstract void SimulateConnection();
    }
}
