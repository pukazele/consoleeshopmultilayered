﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DAL
{
    public class ProductsRepository : IRepository<Product>
    {
        private OrdersContext Db { get; set; }

        public ProductsRepository(OrdersContext context) => Db = context;

        public IEnumerable<Product> GetItems()
        {
            return Db.Products;
        }

        public Product GetItem(int id)
        {
            return Db.Products.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Product item)
        {
            BindEntities(item);

            Db.Products.Add(item);
        }

        public void Update(Product item)
        {
            BindEntities(item);

            int index = Db.Products.FindIndex(x => x.Id == item.Id);
            Db.Products[index] = item;
        }

        public void Delete(int id)
        {
            int index = Db.Products.FindIndex(x => x.Id == id);
            Db.Products.RemoveAt(index);
        }

        private void BindEntities(Product item)
        {
            if (item.OrdersId != null)
            {
                foreach (var orderId in item.OrdersId)
                {
                    if (!Db.Orders.Exists(x => x.Id == orderId)) throw new ArgumentException("There no order(s) with provided Id(s)");

                    var searchedOrder = Db.Orders.FirstOrDefault(x => x.Id == orderId);
                    item.Orders.Add(searchedOrder);
                }
            }
        }
    }
}
