﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DAL
{
    public class OrdersRepository : IRepository<Order>
    {
        private OrdersContext Db { get; set; }

        public OrdersRepository(OrdersContext context) => Db = context;

        public IEnumerable<Order> GetItems()
        {
            return Db.Orders;
        }

        public Order GetItem(int id)
        {
            return Db.Orders.FirstOrDefault(x => x.Id == id);
        }

        public void Create(Order item)
        {
            BindEntities(item);

            Db.Orders.Add(item);

        }

        public void Update(Order item)
        {
            BindEntities(item);
            int index = Db.Orders.FindIndex(x => x.Id == item.Id);
            Db.Orders[index] = item;
        }

        public void Delete(int id)
        {
            int index = Db.Orders.FindIndex(x => x.Id == id);
            Db.Orders.RemoveAt(index);
        }

        private void BindEntities(Order item)
        {
            item.User = Db.Users.FirstOrDefault(x => x.Id == item.Id) ?? throw new ArgumentException("There no user with provided Id");

            if (item.ProductIdAndAmountPairs != null)
            {
                foreach (var pair in item.ProductIdAndAmountPairs)
                {
                    if (!Db.Products.Exists(x => x.Id == pair.Key)) throw new ArgumentException("There no product(s) with provided Id(s)");

                    var searchedProduct = Db.Products.FirstOrDefault(x => x.Id == pair.Key);
                    item.ProductsList.Add(searchedProduct);
                }
            }
        }
    }
}
