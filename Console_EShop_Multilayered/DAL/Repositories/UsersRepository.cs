﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DAL
{
    public class UsersRepository : IRepository<RegisteredUser>
    {
        private OrdersContext Db { get; set; }

        public UsersRepository(OrdersContext context) => Db = context;

        public IEnumerable<RegisteredUser> GetItems()
        {
            return Db.Users;
        }

        public RegisteredUser GetItem(int id)
        {
            return Db.Users.FirstOrDefault(x => x.Id == id);
        }

        public void Create(RegisteredUser item)
        {
            BindEntities(item);

            Db.Users.Add(item);
        }

        public void Update(RegisteredUser item)
        {
            int index = Db.Users.FindIndex(x => x.Id == item.Id);
            Db.Users[index] = item;
        }

        public void Delete(int id)
        {
            int index = Db.Users.FindIndex(x => x.Id == id);
            Db.Users.RemoveAt(index);
        }

        private void BindEntities(RegisteredUser item)
        {
            if (item.OrdersId != null)
            {
                foreach (var orderId in item.OrdersId)
                {
                    if (!Db.Orders.Exists(x => x.Id == orderId)) throw new ArgumentException("There no order(s) with provided Id(s)");

                    var searchedOrder = Db.Orders.FirstOrDefault(x => x.Id == orderId);
                    item.Orders.Add(searchedOrder);
                }
            }
        }
    }
}
