﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public interface IProduct : IEntity
    {
        string Name { get; set; }
        decimal Price { get; set; }
    }
}
