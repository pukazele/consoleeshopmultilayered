﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class Product : IProduct
    {
        private static int _autoId = 1;
        public int Id { get; set; } = _autoId++;
        public string Name { get; set; }
        public decimal Price { get; set; }
        public List<int> OrdersId { get; set; }
        public List<Order> Orders { get; set; }

        public Product()
        {
            OrdersId = new List<int>();
            Orders = new List<Order>();
        }
    }
}
