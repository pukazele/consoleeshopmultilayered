﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public interface IUser : IEntity
    {
        static int AutoId { get; protected set; } = 1;
        string Nickname { get; set; }
        IPermissions Permissions { get; set; }
    }
}
