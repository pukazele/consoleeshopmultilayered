﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public enum Permissions
    {
        ShowProducts,
        SearchProduct,
        Register,
        Login,
        MakeOrder,
        ShowOrders,
        EditProfile,
        Exit,
        ManageUsers,
        AddProducts,
        ChangeProductsInfo,
        ManageOrders
    }
}
