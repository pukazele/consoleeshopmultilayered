﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class AdminPermissions : RegisteredUserPermissions
    {
        public AdminPermissions()
        {
            ListOfPermissions.Add(Permissions.AddProducts);
            ListOfPermissions.Add(Permissions.ChangeProductsInfo);
            ListOfPermissions.Add(Permissions.ManageUsers);
        }
    }
}
