﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class GuestPermissions : IPermissions
    {
        public List<Permissions> ListOfPermissions { get; set; }

        public GuestPermissions()
        {
            ListOfPermissions = new List<Permissions>()
            {
                Permissions.SearchProduct,
                Permissions.ShowProducts,
                Permissions.Login,
                Permissions.Register
            };
        }
    }
}
