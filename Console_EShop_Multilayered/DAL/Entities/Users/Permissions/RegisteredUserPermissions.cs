﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class RegisteredUserPermissions : GuestPermissions
    {
        public RegisteredUserPermissions()
        {
            ListOfPermissions.RemoveAll(x => x == Permissions.Login);
            ListOfPermissions.RemoveAll(x => x == Permissions.Register);

            ListOfPermissions.Add(Permissions.MakeOrder);
            ListOfPermissions.Add(Permissions.ShowOrders);
            ListOfPermissions.Add(Permissions.EditProfile);
            ListOfPermissions.Add(Permissions.Exit);
        }
    }
}
