﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public interface IPermissions
    {
        List<Permissions> ListOfPermissions { get; set; }
    }
}
