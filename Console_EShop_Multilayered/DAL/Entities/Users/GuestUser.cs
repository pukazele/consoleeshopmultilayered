﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class GuestUser : IUser
    {
        public int Id { get; set; }
        public string Nickname { get; set; } 
        public IPermissions Permissions { get; set; }

        public GuestUser()
        {
            Id = -1;
            Nickname = "Guest_";
            Permissions = new GuestPermissions();
        }
    }
}
