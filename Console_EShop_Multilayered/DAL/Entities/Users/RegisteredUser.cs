﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DAL
{
    public class RegisteredUser : GuestUser, IUser
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public List<int> OrdersId { get; set; }
        public List<Order> Orders { get; set; }

        public RegisteredUser()
        {
            Id = IUser.AutoId++;
            Nickname = "User_" + Id;
            Permissions = new RegisteredUserPermissions();
            OrdersId = new List<int>();
            Orders = new List<Order>();
        }
    }
}
