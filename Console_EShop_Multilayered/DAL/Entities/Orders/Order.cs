﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class Order : IEntity
    {
        private static int _autoId = 1;
        public int Id { get; set; }

        public int UserId { get; set; }
        public IUser User { get; set; }

        public Dictionary<int, int> ProductIdAndAmountPairs { get; set; }
        public List<IProduct> ProductsList { get; set; }

        public OrderStatus Status { get; set; }

        public Order()
        {
            Id = _autoId++;
            ProductIdAndAmountPairs = new Dictionary<int, int>();
            ProductsList = new List<IProduct>();
        }
    }
}
