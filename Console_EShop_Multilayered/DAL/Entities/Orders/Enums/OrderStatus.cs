﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public enum OrderStatus
    {
        New,
        CancelledByAdmin,
        PaymentPassed,
        Sent,
        Received,
        Completed,
        CancelledByUser
    }
}
