﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class UnitOfWork
    {
        private readonly OrdersContext db;

        private ProductsRepository productsRepository;
        private UsersRepository usersRepository;
        private OrdersRepository ordersRepository;

        public UnitOfWork() => db = new OrdersContext();

        public ProductsRepository Products
        {
            get
            {
                if (productsRepository is null) productsRepository = new ProductsRepository(db);
                return productsRepository;
            }
        }

        public UsersRepository Users
        {
            get
            {
                if (usersRepository is null) usersRepository = new UsersRepository(db);
                return usersRepository;
            }
        }

        public OrdersRepository Orders
        {
            get
            {
                if (ordersRepository is null) ordersRepository = new OrdersRepository(db);
                return ordersRepository;
            }
        }
    }


}
